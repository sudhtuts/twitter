import axios from 'axios';
import { browserHistory } from 'react-router';
import cookie from 'react-cookie';

import {
  SESSION_PRESENT,
  LOGIN_ERROR,
  CLEAR_ERRORS,
  SESSION_TERMINATION,
  USER_DETAILS
} from './types';

const API_URL = "http://localhost:3000";

export function login({email, password}) {
  return (dispatch) => {
    axios.post(`${API_URL}/users/login.json`, { email, password })
      .then(res => {
        let {token} = res.data;
        cookie.save('twitter-token', token, { path: '/' });
        dispatch({type: SESSION_PRESENT});
        dispatch({type: USER_DETAILS, payload: res.data});
        browserHistory.push("/dashboard");
      }, ({response}) => {
        dispatch(conveyError(response.data.message, LOGIN_ERROR));
      });
  }
}

export function register(data) {
  return (dispatch) => {
    axios.post(`${API_URL}/users.json`, data)
      .then(({data}) => {
        let {token} = data;
        data = data["data"];
        cookie.save('twitter-token', token, { path: '/' });
        dispatch({type: SESSION_PRESENT});
        dispatch({type: USER_DETAILS, payload: data});
        browserHistory.push("/dashboard");
      }, ({response}) => {
        dispatch(conveyError(response.data.message, LOGIN_ERROR));
      });
  }
}

export function logout() {
  cookie.remove('twitter-token', { path: '/' });
  return { type: SESSION_TERMINATION };
}

export function conveyError(message, type) {
  return {
    type,
    payload: message
  };
}

export function clearErrors() {
  return (dispatch) => {
    setTimeout(() => {
      dispatch({
        type: CLEAR_ERRORS,
        payload: null
      });
    }, 4000);
  };
}

export function verifyInitialSession() {
  let token = cookie.load('twitter-token');
  if(!token) {
    return { type: USER_DETAILS, payload: "no_data" };
  }
  else {
    return (dispatch) => {
      axios.get(`${API_URL}/users/currentUser.json?token=${token}`)
        .then(({data}) => {
          dispatch({ type: USER_DETAILS, payload: data });
          dispatch({type: SESSION_PRESENT});
          browserHistory.push("/dashboard");
        }, err => {
          console.log(err);
          dispatch({ type: USER_DETAILS, payload: "no_data" });
        });
    };
  }
}


