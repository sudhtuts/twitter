import {
  SESSION_PRESENT,
  SESSION_TERMINATION,
  LOGIN_ERROR,
  CLEAR_ERRORS,
  USER_DETAILS
} from '../actions/types';

export default (state={}, action) => {
  switch(action.type) {
    case SESSION_PRESENT:
      return { ...state, authenticated: true };
    case USER_DETAILS:
      return { ...state, userDetails: action.payload };  
    case SESSION_TERMINATION:
      return { ...state, authenticated: false };  
    case LOGIN_ERROR:
      return { ...state, message: action.payload };
    case CLEAR_ERRORS:
      return { ...state, userDetails: null };
  }

  return state;
};