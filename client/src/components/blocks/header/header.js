import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';

import style from './header.css';

import LoginModal from './modals/login';
import RegisterModal from './modals/register';
import * as actions from '../../../actions/index';

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showLoginModal: false,
      showRegisterModal: false
    };

    this.toggleLoginModal = this.toggleLoginModal.bind(this);
    this.toggleRegisterModal = this.toggleRegisterModal.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  toggleLoginModal(flag) {
    this.setState({ showLoginModal: flag });
  }

  toggleRegisterModal(flag) {
    this.setState({ showRegisterModal: flag });
  }

  componentDidUpdate() {
    if (this.props.errorMessage) {
      this.props.clearErrors();
    }
    if(this.props.authenticated) {
      if(this.state.showLoginModal)
        this.toggleLoginModal(false);
      else if(this.state.showRegisterModal)  
        this.toggleRegisterModal(false);
    }
  }

  logOut() {
    this.props.logout();
  }

  render() {
    const navbarInstanceNoSession = (
      <div>
        <Navbar fixedTop={true}>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/"><RaisedButton label="Twitter" /></Link>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav pullRight>
            <NavItem onClick={(evt) => this.toggleLoginModal(true)}><RaisedButton primary label="Login" /></NavItem>
            <NavItem onClick={(evt) => this.toggleRegisterModal(true)}><RaisedButton primary label="Register" /></NavItem>
          </Nav>
        </Navbar>
        <LoginModal
          show={this.state.showLoginModal}
          errorMessage={this.props.errorMessage}
          toggleLogin={this.toggleLoginModal} />
        <RegisterModal
          show={this.state.showRegisterModal && (this.props.authenticated !== true)}
          errorMessage={this.props.errorMessage}
          toggleRegister={this.toggleRegisterModal} />  
      </div>
    );

    const navbarInstanceSession = (
      <div>
        <Navbar fixedTop={true}>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/dashboard"><RaisedButton label="Dashboard" /></Link>
            </Navbar.Brand>
          </Navbar.Header>
          <Nav pullRight>
            <NavItem onClick={this.logOut.bind(this)}><RaisedButton primary label="Logout" /></NavItem>
          </Nav>
        </Navbar>
      </div>
    );

    return (
      this.props.authenticated ? navbarInstanceSession : navbarInstanceNoSession
    );
  }
}

function mapStateToProps(state) {
  let { authenticated } = state.auth;
  return {
    errorMessage: state.auth.message,
    authenticated
  };
}

export default connect(mapStateToProps, actions)(Header);

