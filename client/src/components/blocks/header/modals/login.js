import React, { Component } from 'react';
import { reduxForm, Field, filterProps } from 'redux-form';
import { Modal, Button } from 'react-bootstrap'
import { TextField } from 'redux-form-material-ui';

import style from './modal.css'
import * as actions from '../../../../actions/index';

class LoginModal extends Component {

  constructor(props) {
    super(props);
    this.state = { show: this.props.show };
    this.hideModal = this.hideModal.bind(this);
    this.dynamicTitle = this.dynamicTitle.bind(this);
  }

  dynamicTitle() {
    let errorMessage = this.props.errorMessage;
    if(!errorMessage) return ("Login");
    else { 
      return <div className="red">{this.props.errorMessage}</div>;
    }
  }

  hideModal() {
    this.props.toggleLogin(false);
  }

  submit({email, password}) {
    let { dispatch } = this.props;

    dispatch(actions.login({email, password}));

  }

  render() {

    const { handleSubmit, fields: { email, password } } = this.props;
    
    return (
      <Modal
        show={this.props.show}
        onHide={this.hideModal}
        dialogClassName={` ${style.body} `}
        >
        <form onSubmit={handleSubmit(this.submit.bind(this))}>
          <Modal.Header closeButton>
            <Modal.Title
              id="contained-modal-title-lg"
              bsClass={` ${style.center} `}
              >
              {this.dynamicTitle()}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={` ${style.mt5neg} `}>
            
              <div>
                <Field name="email" component={TextField} floatingLabelText="Email" />
              </div>

              <div>
                <Field name="password" type="password" component={TextField} floatingLabelText="Password" />
              </div>
            
          </Modal.Body>
          <Modal.Footer>
            <button action="submit" className="btn btn-primary btn-lg btn-block">Login</button>
          </Modal.Footer>
        </form>
      </Modal>
    );
  }

}

function validate({email, password}) {
  let errors = {};
  if (!email) errors["email"] = "Email is required";
  if (!password) errors["password"] = "Password is required";

  return errors;
}

export default reduxForm({
  form: 'login',
  fields: ['email', 'password'],
  validate
},null,actions)(LoginModal);