import React, { Component } from 'react'

import style from './body.css'

class Body extends Component {
  render() {
    return (
      <div className={` ${style.homeImageContainer} `}>
        <img  className={` ${style.homeImage} `} src="/assets/twitterHome.jpg" />
      </div>
    )
  }
}

export default Body