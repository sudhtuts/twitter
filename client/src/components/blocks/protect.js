import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function (ComposedComponent) {
  class Protection extends Component {

    static contextTypes = {
      router: React.PropTypes.object
    };

    componentWillMount() {
      if (!this.props.authenticated) {
        this.context.router.push('/');
      }
    }

    componentWillUpdate(nextProps, nextState) {
      if (!nextProps.authenticated) {
        this.context.router.push('/');
      }
    }


    render() {
      return (
        <ComposedComponent {...this.props} />
      );
    }
  }

  function mapStateToProps(state) {
    let { authenticated } = state.auth;
    return {
      authenticated
    };
  }
  return connect(mapStateToProps)(Protection);
}