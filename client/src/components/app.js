import React, { Component } from 'react';

import Header from './blocks/header/header';
import Body from './blocks/body/body'
import style from './app.css'

import { connect } from 'react-redux';

class App extends Component {

  showBody() {
    if (!this.props.authenticated) {
      return <Body />;
    }
  }

  showApp() {
    if (this.props.userDetails !== undefined) {
      return (
        <div>
          <Header />
          {this.props.children}
          {this.showBody.call(this)}
        </div>
      );
    }
    else {
      return (
        <div className={` ${style.homeImageContainer} `}>
          <img className={` ${style.homeImage} `} src="/assets/loading.gif" />
        </div>
      )
    }
  }

  render() {
    return (
      <div>
        {this.showApp.call(this)}
      </div>
    );
  }
}

function mapStateToProps(state) {
  let { authenticated, userDetails } = state.auth;
  return {
    authenticated,
    userDetails
  };
}

export default connect(mapStateToProps, null)(App);
