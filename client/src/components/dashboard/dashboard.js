import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Navbar, Nav, NavItem } from 'react-bootstrap'
import RaisedButton from 'material-ui/RaisedButton'

import style from './dashboard.css'
import * as actions from '../../actions/index';

class Dashboard extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let currentUser = this.props.userDetails;
    return (
      <h1 className={` ${style.mt10} `}>Hi {currentUser.first_name+' '+currentUser.last_name}</h1>
    );
  }
}

function mapStateToProps(state) {
  let { userDetails } = state.auth;
  return {
    userDetails
  };
}

export default connect(mapStateToProps, actions)(Dashboard);

