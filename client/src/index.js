import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from 'bootstrap/dist/css/bootstrap.css'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import reduxThunk from 'redux-thunk';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap'
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

import * as actions from './actions';
import reducers from './reducers';
import App from './components/app';
import Protection from './components/blocks/protect';
import Dashboard from './components/dashboard/dashboard';

const createStoreWithMiddleware = applyMiddleware(reduxThunk)(createStore);
const store = createStoreWithMiddleware(reducers);

store.dispatch(actions.verifyInitialSession());

const core = (
  <Provider store={store}>
    <MuiThemeProvider>
      <Router history={browserHistory}>
        <Route path="/" component={App}>
          <Route path="/dashboard" component={Protection(Dashboard)} />
        </Route>
      </Router>
    </MuiThemeProvider>
  </Provider>
);

ReactDOM.render(core, document.getElementById("container"))

