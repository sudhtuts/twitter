Rails.application.routes.draw do
  resources :requests
  resources :connections
  resources :tweets, shallow: true do
    resources :comments
  end
  resources :users do
    collection do
      post :login
      get :currentUser
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
