# Be sure to restart your server when you modify this file.

# ApplicationController.renderer.defaults.merge!(
#   http_host: 'example.org',
#   https: false
# )
module BSON
  class ObjectId
    alias :to_json :to_s
    alias :as_json :to_s
  end
end