class Tweet
  
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user
  has_many :comments, dependent: :destroy

  before_save :set_time_stamps

  field :content

  private
  def set_time_stamps
    self.created_at = Time.now unless self.created_at
    self.updated_at = Time.now
  end
  
end
