class Request
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user

  before_save :set_time_stamps

  field :to_user, type: BSON::ObjectId
  field :rejected, type: Boolean
  field :rejected_at, type: DateTime

  private
  def set_time_stamps
    self.created_at = Time.now unless self.created_at
    self.updated_at = Time.now
  end
end
