class User

  attr_accessor :token

  require 'bcrypt'
  include Mongoid::Document
  include Mongoid::Timestamps
  has_many :tweets, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :connections, dependent: :destroy
  has_many :requests, dependent: :destroy

  before_save :hash_password
  before_save :set_time_stamps

  field :email, type: String
  field :password, type: String
  field :city, type: String
  field :first_name, type: String
  field :last_name, type: String
  

  validates_presence_of :first_name, :message => "First name is empty"
  validates_presence_of :last_name, :message => "Last name is empty"
  validates_presence_of :email, :message => "email is empty"
  validates_presence_of :password, :message => "password is empty"
  validates_presence_of :city, :message => "city is empty"

  validates_uniqueness_of :email

  private
  def hash_password
    secret = "12d32fj384hgh95h4g985jhv98h45h9845hv9854h98vh45vh845@!#$#@#"
    self.password = BCrypt::Password.create(self.password+secret, cost: 5)
  end

  private
  def set_time_stamps
    self.created_at = Time.now unless self.created_at
    self.updated_at = Time.now
  end
  
  public
  def is_valid_password(password)
    secret = "12d32fj384hgh95h4g985jhv98h45h9845hv9854h98vh45vh845@!#$#@#"
    BCrypt::Password.new(self.password) == password+secret
  end
end
