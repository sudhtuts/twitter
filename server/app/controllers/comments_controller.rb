class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]
  before_action :set_tweet, only: [:index, :create]
  before_action :validate_token
  
  # GET /comments
  # GET /comments.json
  def index
    @comments = @tweet.comments
    respond_to do |format|
      format.json { render json: @comments, status: :ok }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @current_user.comments.build(content: params[:content],tweet_id: @tweet.id)

    respond_to do |format|
      if @comment.save
        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
        format.json { render json: @comment, status: :ok }
      else
        format.html { render :new }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update!(content: params[:content])
        @comment.content = params[:content]
        format.json { render json: @comment, status: :ok }
      else
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { render json: @comment, status: :ok }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      begin
        @comment = Comment.find(params[:id])
      rescue Mongoid::Errors::DocumentNotFound  
        render json: { message: "Error" },status: :unprocessable_entity
      end
    end

    def set_tweet
      begin
        @tweet = Tweet.find(params[:tweet_id])
      rescue Mongoid::Errors::DocumentNotFound  
        render json: { message: "Error" },status: :unprocessable_entity
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    # def comment_params
    #   params.require(:comment).permit(:content)
    # end
end
