class ApplicationController < ActionController::Base
  include JwtHelper
  protect_from_forgery with: :null_session
  skip_before_filter  :verify_authenticity_token

  def validate_token
    if(!params[:token])
      render json:{ message: "Token is not present in the request" }, status: :unprocessable_entity 
    elsif !verify_token(params[:token])["valid"]
      render json:{ message: "Token is Expired." }, status: :unauthorized
    else
      @current_user = verify_token(params[:token])["data"]
    end
  end
end
