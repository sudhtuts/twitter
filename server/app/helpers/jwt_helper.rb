module JwtHelper
  require 'jwt'
  
  def get_secret
    '#$$#V%$V#$vrbt45b45$%^H$%H$%h45h45~~~~~~~~@'
  end

  def generate_token(id)
    exp = (Time.now+2.hours).to_i
    payload = {:id => id.to_s, :exp => exp}
    JWT.encode payload, get_secret, 'HS256'
  end

  def verify_token(token)
    result = {}
    begin
      valid = JWT.decode token, get_secret, true, { :algorithm => 'HS256' }
      result["data"] = User.find(valid[0]["id"])
      result["valid"] = true
      return result
    rescue Exception
      result["data"] = {}
      result["valid"] = false
      return result
    end
  end
end