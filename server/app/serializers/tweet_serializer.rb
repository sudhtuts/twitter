class TweetSerializer < ActiveModel::Serializer
  belongs_to :user
  attributes :id, :content, :created_at, :updated_at
end
