class CommentSerializer < ActiveModel::Serializer
  belongs_to :user
  belongs_to :tweet
  attributes :id, :content, :created_at, :updated_at
end
